<?php

/**
 * @param int $limit
 *    The number of items the function should process
 *
 * @return bool
 */
function sharepoint_rest_pull_sharepoint_changes($limit = 0) {

  $sync_enabled = variable_get('sharepoint_rest_sync_enabled', FALSE);
  if (empty($sync_enabled)) {
    return FALSE;
  }

  $last_sync = variable_get('sharepoint_rest_last_sync');
  $entity_maps = sharepoint_rest_sp_to_drupal_maps(TRUE);
  foreach($entity_maps as $entity_map) {

    $sp_list_type = $entity_map['sharepoint']['list_type'];
    $last_sync_token = !empty($last_sync[$sp_list_type]['token']) ? $last_sync[$sp_list_type]['token'] : NULL;

    $sp_list = new SharePointRESTList($sp_list_type);
    $changes = $sp_list->getChanges($last_sync_token);
    if (empty($changes) || !is_array($changes)) {
      watchdog('sharepoint_rest', 'SharePoint sync for @list_type triggered: no items to synchronize.', array(
        '@list_type' => $sp_list_type,
      ));
      continue;
    }

    // Limit number of items to process
    if (!empty($limit) && $limit > 0) {
      $changes = array_slice($changes, 0, $limit);
    }

    $created_or_updated_or_restored = array();
    $deleted = array();
    foreach ($changes as $change) {
      if (in_array($change->ChangeType, array(1, 2, 7)) && empty($created_or_updated_or_restored[$change->ItemId])) {
        $filters = module_invoke_all('sharepoint_rest_pull_changes_filter', $change, $entity_map);
        $filter = rawurlencode(implode('', $filters));
        $select = rawurlencode('Id,GUID');
        $list = new SharePointRESTList($sp_list_type);
        $items = $list->getItems('$select=' . $select . '&$filter=' . $filter);
        if (!empty($items) && is_array($items)) {
          foreach ($items as $item) {
            module_load_include('batch.inc', 'sharepoint_rest');
            sharepoint_rest_batch_operation_migrate_to_drupal($item, $entity_map, $context);
          }
        }

        unset($deleted[$change->ItemId]);
        $created_or_updated_or_restored[$change->ItemId] = $change->ItemId;
      }
      elseif ($change->ChangeType == 3 && empty($deleted[$change->ItemId])) {
        unset($created_or_updated_or_restored[$change->ItemId]);
        $deleted[$change->ItemId] = $change->ItemId;

        $entities = sharepoint_rest_api_get_map_by_sp_item($sp_list_type, $change->ItemId);
        if (!empty($entities)) {
          foreach ($entities as $drupal_entity) {
            module_invoke_all('sharepoint_rest_pull_entity_delete', $drupal_entity);
          }
        }
      }

      // Save last sync token after every operation. It's helpful when
      // cron can't handle huge amount of resources to process at once,
      // but it still makes some progress on each invoke and eventually can
      // get all items processed.
      $last_sync[$sp_list_type] = array(
        'token' => $change->ChangeToken->StringValue,
        'date' => strtotime($change->Time),
      );
      variable_set('sharepoint_rest_last_sync', $last_sync);
    }

    watchdog('sharepoint_rest', 'SharePoint date sync triggered: items of type @type were synchronized.',
      array('@type' => $sp_list_type));
  }
}
