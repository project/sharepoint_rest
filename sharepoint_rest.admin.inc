<?php

/**
 * @file
 *
 * TODO: Describe.
 */

/**
 *  Implements system_settings_form() for module settings configuration.
 */
function sharepoint_rest_admin_settings_form($form, &$form_state) {
  $form['sharepoint_rest_base_url'] = array(
    '#title' => t('Sharepoint base URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('sharepoint_rest_base_url', NULL),
    '#description' => t('Base url of your sharepoint domain. EG : https://xxxxx.sharepoint.com/sites/xxxx'),
    '#required' => TRUE,
  );

  $form['sharepoint_rest_username'] = array(
    '#title' => t('Sharepoint username'),
    '#type' => 'textfield',
    '#default_value' => variable_get('sharepoint_rest_username', NULL),
    '#description' => t('Username to access the documents from sharepoint.'),
    '#required' => TRUE,
  );

  $form['sharepoint_rest_password'] = array(
    '#title' => t('Sharepoint password'),
    '#type' => 'textfield',
    '#default_value' => variable_get('sharepoint_rest_password', NULL),
    '#description' => t('Password to access the documents from sharepoint.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * TODO: Describe.
 */
function sharepoint_rest_sp_to_drupal_migration_form($form, &$form_state) {

  $available_maps = sharepoint_rest_sp_to_drupal_maps();
  $options = array();
  foreach ($available_maps as $available_map) {
    $options[] = t('Migrate @sharepoint to @drupal.', array(
      '@sharepoint' => $available_map['sharepoint']['list_type'],
      '@drupal' => $available_map['drupal']['entity_type'] . ':' . $available_map['drupal']['entity_bundle'],
    ));
  }

  $form['map'] = array(
    '#type' => 'select',
    '#title' => t('Choose data mapping'),
    '#options' => $options,
    '#default_value' => 0,
    '#required' => TRUE,
  );

  $form['limit'] = array(
    '#title' => t('Limit'),
    '#type' => 'textfield',
    '#default_value' => '1',
    '#description' => t('Enter amount of SP items to be, enter 0 to migrate all.'),
    '#element_validate' => array('element_validate_integer'),
  );

  $form['debug'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debug'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['debug']['item_id'] = array(
    '#title' => t('Item ID'),
    '#type' => 'textfield',
    '#description' => t('Enter SP item ID to migrate to Drupal.'),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
  );

  return $form;
}

/**
 * TODO: Describe.
 */
function sharepoint_rest_sp_to_drupal_migration_form_submit($form, &$form_state) {

  $map_key = $form_state['values']['map'];
  $available_maps = sharepoint_rest_sp_to_drupal_maps();
  $entity_map = $available_maps[$map_key];

  $list = new SharePointRESTList($entity_map['sharepoint']['list_type']);

  $select = rawurlencode('Id,GUID');
  $filter = !empty($entity_map['sharepoint']['filter']) ? rawurlencode($entity_map['sharepoint']['filter']) : '';

  $amount = 1000;
  if (!empty($form_state['values']['limit'])) {
    $amount = $form_state['values']['limit'];
  }

  if (!empty($form_state['values']['item_id'])) {
    if (empty($filter)) {
      $filter = rawurlencode('Id eq ' . $form_state['values']['item_id']);
    }
    else {
      $filter .= rawurlencode(' and Id eq ' . $form_state['values']['item_id']);
    }
  }

  $items = $list->getItems('$select=' . $select . '&$filter=' . $filter . '&$top=' . $amount);

  if (empty($items) || !is_array($items)) {
    drupal_set_message(t('Could not find items in SharePoint.'), 'warning');
    return;
  }

  $operations = array();
  foreach ($items as $item) {
    $operations[] = array('sharepoint_rest_batch_operation_migrate_to_drupal', array($item, $entity_map));
  }

  $batch = array(
    'operations' => $operations,
    'finished' => 'sharepoint_rest_batch_operation_finished',
    'file' => drupal_get_path('module', 'sharepoint_rest') . '/sharepoint_rest.batch.inc',
  );

  batch_set($batch);
}

/**
 * TODO: Describe.
 * Form for migration from Drupal to SP.
 */
function sharepoint_rest_drupal_to_sp_migration_form($form, &$form_state) {

  $available_maps = sharepoint_rest_drupal_to_sp_maps();
  $options = array();
  foreach ($available_maps as $available_map) {
    $options[] = t('Migrate @drupal to @sharepoint.', array(
      '@sharepoint' => $available_map['sharepoint']['list_type'],
      '@drupal' => $available_map['drupal']['entity_type'] . ':' . $available_map['drupal']['entity_bundle'],
    ));
  }

  $form['map'] = array(
    '#type' => 'select',
    '#title' => t('Choose data mapping'),
    '#options' => $options,
    '#default_value' => 0,
    '#required' => TRUE,
  );

  $form['limit'] = array(
    '#title' => t('Limit'),
    '#type' => 'textfield',
    '#default_value' => '1',
    '#description' => t('Enter no of nodes to be migrated, enter 0 to migrate all.'),
    '#element_validate' => array('element_validate_integer'),
  );

  $form['debug'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debug'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['debug']['entity_id'] = array(
    '#title' => t('Entity ID'),
    '#type' => 'textfield',
    '#description' => t('Enter entity ID to migrate to SharePoint.'),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Migrate Drupal Entities to SharePoint')
  );

  return $form;
}

/**
 * Submit handler for migration from Drupal to SP.
 */
function sharepoint_rest_drupal_to_sp_migration_form_submit($form, $form_state) {

  $map_key = $form_state['values']['map'];
  $available_maps = sharepoint_rest_drupal_to_sp_maps();
  $entity_map = $available_maps[$map_key];

  $entity_type = $entity_map['drupal']['entity_type'];
  $entity_bundle = $entity_map['drupal']['entity_bundle'];

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entity_type);
  $query->entityCondition('bundle', $entity_bundle);

  if (!empty($form_state['values']['limit'])) {
    $query->range(0, $form_state['values']['limit']);
  }

  if (!empty($form_state['values']['entity_id'])) {
    $query->entityCondition('entity_id', $form_state['values']['entity_id']);
  }

  $entities = $query->execute();

  $operations = array();
  foreach($entities[$entity_type] as $entity_id => $entity) {
    $operations[] = array('sharepoint_rest_batch_operation_migrate_to_sharepoint', array($entity_id, $entity_map));
  }

  if (!empty($operations)) {

    $batch = array(
      'operations' => $operations,
      'finished' => 'sharepoint_rest_batch_operation_finished',
      'file' => drupal_get_path('module', 'sharepoint_rest') . '/sharepoint_rest.batch.inc',
    );

    batch_set($batch);
  }
  else {
    drupal_set_message(t('No entities to migrate.'), 'warning');
  }
}

/**
 * TODO: Describe.
 */
function sharepoint_rest_cron_sync_form($form) {

  $form['sync'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automatic synchronization'),
  );

  $sync_enabled = variable_get('sharepoint_rest_sync_enabled', FALSE);
  $form['sync']['status'] = array(
    '#type' => 'item',
    '#title' => t('Status'),
    '#markup' => !empty($sync_enabled) ? t('Enabled') : t('Disabled'),
  );

  $last_sync = variable_get('sharepoint_rest_last_sync', array());
  $entity_maps = sharepoint_rest_sp_to_drupal_maps(TRUE);
  foreach ($entity_maps as $entity_map) {

    $list_type = $entity_map['sharepoint']['list_type'];
    $entity_type = $entity_map['drupal']['entity_type'];
    $entity_bundle = $entity_map['drupal']['entity_bundle'];

    $form['info'][$list_type] = array(
      '#type' => 'fieldset',
      '#title' => t('Migration of @list to Drupal @entity.', array(
        '@list' => $list_type,
        '@entity' => $entity_type . ':' . $entity_bundle,
      ))
    );

    $last_list_sync = !empty($last_sync[$list_type]) ? $last_sync[$list_type] : FALSE;
    $form['info'][$list_type]['last_sync'] = array(
      '#type' => 'item',
      '#title' => t('Date when the last SharePoint change was synchronized with Drupal'),
      '#markup' => $last_list_sync ? format_date($last_list_sync['date'], 'medium') : t('Never'),
    );

    $form['info'][$list_type]['change_token'] = array(
      '#type' => 'item',
      '#title' => t('Change token'),
      '#markup' => $last_list_sync ? $last_list_sync['token'] : t('None'),
    );
  }

  $form['actions']['#type'] = 'actions';

  if (!empty($sync_enabled)) {
    $form['actions']['disable_sync'] = array(
      '#type' => 'submit',
      '#value' => t('Disable synchronization'),
      '#submit' => array('sharepoint_rest_cron_sync_form_disable_sync'),
    );
  }
  else {
    $form['actions']['enable_sync'] = array(
      '#type' => 'submit',
      '#value' => t('Enable synchronization'),
      '#submit' => array('sharepoint_rest_cron_sync_form_enable_sync'),
    );
  }

  $form['actions']['sync'] = array(
    '#type' => 'submit',
    '#value' => t('Sync now'),
    '#submit' => array('sharepoint_rest_cron_sync_form_sync_now'),
  );

  return $form;
}

function sharepoint_rest_cron_sync_form_disable_sync() {
  variable_set('sharepoint_rest_sync_enabled', FALSE);
}

/**
 * Sets the variable to enable sync.
 *
 * It will get the latest change token when sync is enabled, which means only
 * changes after sync is enabled will be respected.
 */
function sharepoint_rest_cron_sync_form_enable_sync() {
  variable_set('sharepoint_rest_sync_enabled', TRUE);
  $last_sync = variable_get('sharepoint_rest_last_sync', array());
  $entity_maps = sharepoint_rest_sp_to_drupal_maps(TRUE);
  foreach ($entity_maps as $entity_map) {
    $list_type = $entity_map['sharepoint']['list_type'];
    $sp_list = new SharePointRESTList($list_type);
    $change_fetch_limit = 1000;
    $latest_token_string = NULL;
    // Get the latest token
    do {
      $changes = $sp_list->getChanges($latest_token_string);
      if (!empty($changes) && is_array($changes)) {
        $last_change = array_pop($changes);
        $latest_token_string = $last_change->ChangeToken->StringValue;
        $last_sync[$list_type] = array(
          'token' => $last_change->ChangeToken->StringValue,
          'date' => strtotime($last_change->Time),
        );
      }
    } while (count($changes) == $change_fetch_limit-1);
    variable_set('sharepoint_rest_last_sync', $last_sync);
  }
}

/**
 * TODO: Describe.
 */
function sharepoint_rest_cron_sync_form_sync_now() {

  $sync_enabled = variable_get('sharepoint_rest_sync_enabled', FALSE);
  if (empty($sync_enabled)) {
    drupal_set_message(t('Enable the synchronization first.'), 'warning');
    return;
  }

  module_load_include('cron.inc', 'sharepoint_rest');
  sharepoint_rest_pull_sharepoint_changes(10);
}

/**
 * TODO: Describe.
 */
function sharepoint_rest_debug_info_form($form, &$form_state) {

  $client = SharePointREST::getClient();
  if (empty($client)) {
    drupal_set_message(t('Could not connect to SharePoint. Check the module settings.'), 'warning');
    return $form;
  }

  $sp_web = new SharePointRESTWeb();
  $lists = $sp_web->getLists();
  if (empty($lists)) {
    drupal_set_message(t('Could not find list types in SharePoint. Check the SharePoint settings.'), 'warning');
    return $form;
  }

  $options = array();
  foreach ($lists as $list) {
    $options[$list->Title] = $list->Title;
  }
  natcasesort($options);

  $form['list_type'] = array(
    '#type' => 'select',
    '#title' => t('Show available fields for the specified SharePoint List Type'),
    '#options' => $options,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Show fields'),
  );

  return $form;
}

/**
 * TODO: Describe.
 */
function sharepoint_rest_debug_info_form_submit($form, &$form_state) {
  $list = new SharePointRESTList($form_state['values']['list_type']);
  $fields = $list->getFields();

  if (empty($fields) && !is_array($fields)) {
    drupal_set_message(t('The selected List Type doesn\'t have fields.'), 'warning');
    return;
  }

  drupal_set_message(t('List Type %list_type has the following fields available:', array(
    '%list_type' => $form_state['values']['list_type'],
  )));

  foreach ($fields as $field) {
    drupal_set_message(t('%title (machine name: %machine_name)', array(
      '%title' => $field->Title,
      '%machine_name' => $field->InternalName
    )));
  }
}
