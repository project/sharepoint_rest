<?php

/**
 * @file
 * Includes description of hooks available in the module.
 */

/**
 * Defines mapping from Sharepoint items to Drupal entities.
 *
 * @return array
 *   An un-keyed array of settings arrays. Each settings array defines the
 *   following:
 *   - cron_sync: (optional) If true, this mapping will synced on cron.
 *   - sharepoint: Sharepoint source information.
 *     - list_type: The Sharepoint list type to pull items from.
 *     - filter: A Sharepoint filter string to filter the items by.
 *   - drupal: Drupal destination information.
 *     - entity_type: The entity type in drupal to map data into.
 *     - endity_bundle: The bundle to map data into.
 *   - field_map: A collection of arrays mapping Sharepoint fields to Drupal
 *     fields/properties.
 *     - sharepoint_name: (optional) The machine name of the Sharepoint field.
 *       If ommitted, the array key will be used.
 *     - drupal_name: (optional) The machine name of the Drupal field/property.
 *       If omitted, the array key will be used.
 *     - property: (optional) If TRUE, the mapping is for a Drupal property. If
 *       omitted or FALSE, the mapping defines a field.
 *     - html: (optional) If TRUE, the data coming from Sharepoint should be
 *       treated as HTML. If omitted or FALSE, it will be considered plaintext.
 *     - processor: (optional) A callback to process the data from Sharepoint
 *       before it gets sent to the field
 *       (see includes/field_processors/drupal.inc for examples).
 *   - file: (optional) A file containing the processor functions.
 */
function hook_sharepoint_rest_sp_to_drupal_mapping() {
  $entity_map = array();

  $entity_map[] = array(
    'cron_sync' => TRUE,
    'sharepoint' => array(
      'list_type' => 'Publications',
      'filters' => 'External eq 1',
    ),
    'drupal' => array(
      'entity_type' => 'node',
      'entity_bundle' => 'resource',
    ),
    'field_map' => array(
      'Title' => array(
        'drupal_name' => 'title',
        'property' => TRUE,
      ),
      'description' => array(
        'drupal_name' => 'body',
        'html' => TRUE,
      ),
      'content_type' => array(
        'drupal_name' => 'field_resource_content_type',
      ),
      'document_type' => array(
        'drupal_name' => 'field_resource_document_type',
      ),
      'locations' => array(
        'drupal_name' => 'field_resource_locations',
      ),
    ),
  );

  return $entity_map;
}

/**
 * Return a filter string, which will be passed into the query to SP.
 *
 * @param $change object
 *    The change object.
 * @param array $entity_map
 *   The sp-to-drupal mapping being used for this set of changes.
 * @return string The filter string
 *    The filter string.
 */
function hook_sharepoint_rest_pull_changes_filter($change, $entity_map) {
  $filter = 'Id eq ' . $change->ItemId;
  $filter .= ' and External eq 1';
  return $filter;
}

/**
 * Is invoked when pulling from SP and the item has been deleted.
 * 
 * @param $map_entity object
 *   The mapping entity between SP and DP
 */
function hook_sharepoint_rest_pull_entity_delete($map_entity) {
  $dp_entity = entity_load_single($map_entity->entity_type, $map_entity->entity_id);
  $dp_entity->status = 0;
  // Set a flag to know it was saved from sync
  $dp_entity->sharepoint_migrate_flag = TRUE;
  entity_save($map_entity->entity_type, $dp_entity);
}
