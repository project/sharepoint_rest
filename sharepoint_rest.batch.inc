<?php

/**
 * @file
 *
 */


/**
 * Batch operation: move SharePoint items to Drupal.
 */
function sharepoint_rest_batch_operation_migrate_to_drupal($item, $entity_map, &$context) {

  $migration_mode = variable_get('sharepoint_rest_entity_migrate_mode', SHAREPOINT_REST_ENTITY_MIGRATE_MODE_UPDATE);
  $referenced_entities = sharepoint_rest_api_get_map_by_uuid($item->GUID);

  $context['results']['total'][] = $item->Id;
  $context['message'] = t('Processed SharePoint entity with ID @ID', array('@ID' => $item->Id));

  // If the SP entity already have related entity in Drupal and configuration
  // is set to skip already existing content - then nothing to do here.
  $entity = NULL;
  if (!empty($referenced_entities) && $migration_mode == SHAREPOINT_REST_ENTITY_MIGRATE_MODE_SKIP) {
    return;
  }
  elseif (!empty($referenced_entities) && $migration_mode == SHAREPOINT_REST_ENTITY_MIGRATE_MODE_UPDATE) {
    $entity = array_shift($referenced_entities);
    $entity = entity_load_single($entity->entity_type, $entity->entity_id);
  }

  $sharepoint_fields = array();
  $sharepoint_html_fields = array();
  foreach ($entity_map['field_map'] as $field_name => $field_map) {
    $field = !empty($field_map['sharepoint_name']) ? $field_map['sharepoint_name'] : $field_name;
    if (!empty($field_map['html'])) {
      $sharepoint_html_fields[] = $field;
    }
    else {
      $sharepoint_fields[] = $field;
    }
  }

  $sp_list_type = $entity_map['sharepoint']['list_type'];
  $sp_item = new SharePointRESTListItem($sp_list_type);

  $data = array();
  if (!empty($sharepoint_fields)) {
    $response = $sp_item->getSpecificFieldsAsText($item->Id, $sharepoint_fields);
    if (!empty($response)) {
      $data += (array) $response;
    }
  }

  if (!empty($sharepoint_html_fields)) {
    $response = $sp_item->getSpecificFieldsAsHtml($item->Id, $sharepoint_html_fields);
    if (!empty($response)) {
      $data += (array) $response;
    }
  }

  $entity = sharepoint_rest_migrate_to_drupal($data, $entity_map, $entity);
  if (!empty($entity)) {
    $entity_type = $entity_map['drupal']['entity_type'];
    list($entity_id,,) = entity_extract_ids($entity_type, $entity);
    sharepoint_rest_api_add_map($entity_type, $entity_id, $item->GUID, $sp_list_type, $item->Id);
  }
  else {
    $context['results']['failed'][] = $item->Id;
  }
}


/**
 * Batch operation: move Drupal entity to SharePoint.
 */
function sharepoint_rest_batch_operation_migrate_to_sharepoint($entity_id, $entity_map, &$context) {

  $entity_type = $entity_map['drupal']['entity_type'];
  $migration_mode = variable_get('sharepoint_rest_entity_migrate_mode', SHAREPOINT_REST_ENTITY_MIGRATE_MODE_UPDATE);
  $referenced_items = sharepoint_rest_api_get_map($entity_type, $entity_id);

  $context['results']['total'][] = $entity_id;
  $context['message'] = t('Processed drupal entity @type with ID @ID', array('@type' => $entity_type, '@ID' => $entity_id));

  // If the SP entity already have related entity in Drupal and configuration
  // is set to skip already existing content - then nothing to do here.
  $sp_item = NULL;
  if (!empty($referenced_items) && $migration_mode == SHAREPOINT_REST_ENTITY_MIGRATE_MODE_SKIP) {
    return;
  }
  elseif (!empty($referenced_items) && $migration_mode == SHAREPOINT_REST_ENTITY_MIGRATE_MODE_UPDATE) {
    $sp_item = array_shift($referenced_items);
  }

  $sp_item = sharepoint_rest_migrate_to_sharepoint($entity_id, $entity_map, $sp_item);
  if (empty($sp_item)) {
    $context['results']['failed'][] = $entity_id;
  }
}

/**
 * TODO: Describe.
 */
function sharepoint_rest_batch_operation_finished($success, $results, $operations) {
  if ($success) {
    // Display the number of entities we processed.
    drupal_set_message(t('Processed @count items.', array('@count' => count($results['total']))));

    if (!empty($results['failed'])) {
      drupal_set_message(t('Could not migrate entities with the following ids: %list',
        array('%list' => implode(', ', $results['failed']))
      ), 'warning');
    }
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array('@operation' => $error_operation[0], '@args' => print_r($error_operation[0], TRUE))));
  }
}
