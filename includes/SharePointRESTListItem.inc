<?php

/**
 * @file
 * Contains REST API methods for integration with SharePoint.
 */

/**
 * Wraps SPO Client to add new methods to the REST API.
 */
class SharePointRESTListItem {

  protected $service;

  protected $listName;

  protected $endpoint = '/_api/web/Lists';


  public function __construct($list_name) {
    $this->service = SharePointREST::getClient();
    $this->listName = $list_name;
  }


  public function get($item_id) {
    $options = array(
      'id' => $item_id,
    );

    $response = $this->request($options);
    return !empty($response->d) ? $this->stripDirtyChars($response->d) : FALSE;
  }


  public function getAsText($item_id) {

    $options = array(
      'id' => $item_id,
      'endpath' => 'FieldValuesAsText',
    );

    $response = $this->request($options);
    return !empty($response->d) ? $this->stripDirtyChars($response->d) : FALSE;
  }


  public function getAsHtml($item_id) {

    $options = array(
      'id' => $item_id,
      'endpath' => 'FieldValuesAsHtml',
    );

    $response = $this->request($options);
    return !empty($response->d) ? $this->stripDirtyChars($response->d) : FALSE;
  }


  public function getSpecificFields($item_id, $fields) {

    $options = array(
      'id' => $item_id,
      'query' => '$select=' . implode(',', $fields),
    );

    $response = $this->request($options);
    return !empty($response->d) ? $this->stripDirtyChars($response->d) : FALSE;
  }


  public function getSpecificFieldsAsText($item_id, $fields) {

    $options = array(
      'id' => $item_id,
      'endpath' => 'FieldValuesAsText',
      'query' => '$select=' . implode(',', $fields),
    );

    $response = $this->request($options);
    return !empty($response->d) ? $this->stripDirtyChars($response->d) : FALSE;
  }


  public function getSpecificFieldsAsHtml($item_id, $fields) {

    $options = array(
      'id' => $item_id,
      'endpath' => 'FieldValuesAsHtml',
      'query' => '$select=' . implode(',', $fields),
    );

    $response = $this->request($options);
    return !empty($response->d) ? $this->stripDirtyChars($response->d) : FALSE;
  }


  public function create($fields) {
    $options = array(
      'data' => $fields,
      'method' => 'POST',
    );

    $options['data'] += array(
      '__metadata' => array('type' => $this->getListEntityType()),
    );

    $response = $this->request($options);
    return !empty($response->d->Id) ? $response->d->Id : FALSE;
  }


  public function createWithFile($filename, $file_content, $fields) {
    $list = new SharePointRESTList($this->listName);
    $file_uploaded = $list->uploadFile($filename, $file_content);
    if (empty($file_uploaded) || empty($file_uploaded->ServerRelativeUrl)) {
      return FALSE;
    }

    $web = new SharePointRESTWeb();
    $file = $web->getFileFields($file_uploaded->ServerRelativeUrl);
    if (empty($file) || empty($file->Id)) {
      return FALSE;
    }

    $response = $this->update($file->Id, $fields);
    return $response === NULL ? $file : FALSE;
  }


  public function update($item_id, $data) {

    $options = array(
      'id' => $item_id,
      'method' => 'POST',
      'xhttpmethod' => 'MERGE',
      'etag' => '*',
      'data' => $data,
    );

    $options['data'] += array(
      '__metadata' => array('type' => $this->getListEntityType()),
    );

    return $this->request($options);
  }


  public function getListEntityType() {
    $list_names = variable_get('sharepoint_rest_list_entity_names', array());
    if (empty($list_names[$this->listName])) {
      $list = new SharePointRESTList($this->listName);
      $list_entity_name = $list->getEntityTypeName();
      if (!empty($list_entity_name) && !empty($list_entity_name->ListItemEntityTypeFullName)) {
        $list_names[$this->listName] = $list_entity_name->ListItemEntityTypeFullName;
        variable_set('sharepoint_rest_list_entity_names', $list_names);
      }
      else {
        // If we couldn't get the entity type name for some reason then
        // try a fallback name. It might not work for some cases, but that's
        // better than nothing :)
        $list_names[$this->listName] = 'Sp.Data.' . $this->listName . 'ListItem';
      }
    }

    return $list_names[$this->listName];
  }


  protected function stripDirtyChars($response) {
    $clean_response = new stdClass();
    foreach ($response as $key => $value) {
      $dirty_chars = variable_get('sharepoint_rest_dirty_chars', array('x005f_', 'x0020_', 'OData_'));
      $key = str_replace($dirty_chars, '', $key);
      $clean_response->$key = $value;
    }
    return $clean_response;
  }


  protected function request($options = array()) {

    $options += array(
      'endpoint' => $this->endpoint . "/getByTitle('{$this->listName}')/Items",
      'method' => 'GET',
    );

    if (!empty($options['id'])) {
      $options['endpoint'] .= '(' . $options['id'] . ')';
    }

    return $this->service->request($options);
  }

}
