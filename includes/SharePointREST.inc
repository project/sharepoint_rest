<?php

/**
 * @file
 * Contains main class for SharePoint REST API integration.
 */

class SharePointREST {

  /**
   * TODO: Describe.
   */
  private static $client;

  /**
   * TODO: Describe.
   */
  private function __construct() {}

  /**
   * TODO: Describe.
   */
  private function __clone() {}

  /**
   * TODO: Describe.
   */
  static public function getClient() {
    if (is_null(self::$client)) {
      try {

        // Initialize API variables.
        $url = variable_get('sharepoint_rest_base_url');
        $username = variable_get('sharepoint_rest_username');
        $password = variable_get('sharepoint_rest_password');

        if (empty($url)) {
          throw new Exception(t('Sharepoint REST URL is not defined.'));
        }

        if (empty($username) || empty($password)) {
          throw new Exception(t('SharePoint REST credentials are not defined.'));
        }

        self::$client = new SharePointRestClient($url);

        // Make API call to SharePoint to authorize the user. If something will
        // go wrong - the method will throw php error.
        self::$client->signIn($username, $password);
      }
      catch (Exception $e) {
        self::logError($e->getMessage());
        return FALSE;
      }
    }

    return self::$client;
  }

  /**
   * TODO: Describe.
   */
  static public function logError($message) {
    drupal_set_message('SharePoint REST error: ' . $message, 'error');
  }
}
