<?php

/**
 * @file
 * Contains REST API methods for integration with SharePoint.
 */

/**
 * Wraps SPO Client to add new methods to the REST API.
 */
class SharePointRESTList  {

  protected $endpoint = '/_api/web/Lists';

  protected $service;

  protected $name;


  public function __construct($name) {
    $this->service = SharePointREST::getClient();
    $this->name = $name;
  }


  public function get() {
    $response = $this->request();
    return !empty($response) ? $response : FALSE;
  }


  public function getFields() {
    $options['endpath'] = 'fields';
    $response = $this->request($options);
    return !empty($response->d->results) ? $response->d->results : FALSE;
  }


  public function getField($field_guid) {
    $options['endpath'] = 'fields(\'' . $field_guid . '\')';
    $response = $this->request($options);
    return !empty($response->d->results) ? $response->d->results : FALSE;
  }


  public function getEntityTypeName() {
    $options['endpath'] = 'ListItemEntityTypeFullName';
    $response = $this->request($options);
    return !empty($response->d) ? $response->d : FALSE;
  }


  public function getChanges($start_token = NULL) {

    $options = array(
      'endpath' => 'GetChanges',
      'method' => 'POST',
      'data' => array(
        'query' => array(
          '__metadata' => (object) array(
            'type' => 'SP.ChangeQuery',
          ),
          'Add' => TRUE,
          'DeleteObject' => TRUE,
          'Update' => TRUE,
          'Restore' => TRUE,
          'Item' => TRUE,
          'File' => TRUE,
        ),
      )
    );

    if (!empty($start_token)) {
      $options['data']['query']['ChangeTokenStart'] = array(
        '__metadata' => array(
          'type' => 'SP.ChangeToken',
        ),
        'StringValue' => $start_token,
      );
    }

    $response = $this->request($options);
    return !empty($response->d->results) ? $response->d->results : FALSE;
  }


  public function getChangesSinceToken($token) {

    $options = array(
      'endpath' => 'GetListItemChangesSinceToken',
      'method' => 'POST',
      'data' => array(
        'query' => array(
          '__metadata' => (object) array(
            'type' => 'SP.ChangeLogItemQuery',
          ),
          'ChangeToken' => $token,
          'RowLimit' => '5',
          /*'Query' =>
            "<Where>
      <Contains>
         <FieldRef Name='Title' />
         <Value Type='Text'>Te</Value>
      </Contains></Where>",
          'QueryOptions' =>
            '<QueryOptions>
      <IncludeMandatoryColumns>FALSE</IncludeMandatoryColumns>
      <DateInUtc>False</DateInUtc>
      <IncludePermissions>TRUE</IncludePermissions>
      <IncludeAttachmentUrls>FALSE</IncludeAttachmentUrls></QueryOptions>',
          'Contains' =>
            '<Contains>
      <FieldRef Name="Title"/>
      <Value Type="Text">Testing</Value></Contains>'*/
        ),

    /**/
      ),
    );
    return $this->request($options);
  }


  public function uploadFile($filename, $file_content) {
    $options = array(
      'endpath' => 'RootFolder/Files/Add(overwrite=true,url=\'' . rawurlencode($filename) . '\')',
      'method' => 'POST',
      'data' => $file_content,
    );
    $response = $this->request($options);
    return !empty($response->d) ? $response->d : FALSE;
  }


  public function exists() {
    $response = $this->request();
    return !empty($response) ? TRUE : FALSE;
  }


  public function getItems($query = '') {
    $options = array(
      'endpath' => 'Items',
      'query' => !empty($query) ? $query : '',
    );

    $response = $this->request($options);
    return !empty($response->d->results) ? $response->d->results : FALSE;
  }


  public function getContentTypeName($content_type_id) {
    $options['endpath'] = "contenttypes('{$content_type_id}')/name";
    $response = $this->request($options);
    return !empty($response->d->Name) ? $response->d->Name : FALSE;
  }


  protected function request($options = array()) {

    $options += array(
      'endpoint' => $this->endpoint . "/getByTitle('{$this->name}')",
      'method' => 'GET',
    );

    return $this->service->request($options);
  }

}
