<?php

class SharePointRestClient extends SPOClient {

  /**
   * TODO: Describe.
   */
  public function request($options, $pass_form_digest = TRUE) {

    if (!empty($options['endpath'])) {
      $options['endpoint'] .= '/' . $options['endpath'];
    }

    if (!empty($options['query'])) {
      $options['endpoint'] .= '?' . $options['query'];
    }

    if (!empty($options['endpoint'])) {
      $options['url'] = $this->url . $options['endpoint'];
    }

    try {
      $response = parent::request($options, $pass_form_digest);
    }
    catch (Exception $e) {
      SharePointREST::logError($e->getMessage());
      return FALSE;
    }

    return $response;
  }
}
