<?php

/**
 * @param $value
 * @param $field_info
 * @param $entity_map
 * @return array
 */
function sharepoint_rest_drupal_processor_content_type($value, $field_info, $entity_map) {

  $content_types = variable_get('sharepoint_rest_sp_content_types', array());
  if (empty($content_types[$value])) {
    $list = new SharePointRESTList($entity_map['sharepoint']['list_type']);
    $content_type = $list->getContentTypeName($value);
    $content_types[$value] = $content_type;
    variable_set('sharepoint_rest_sp_content_types', $content_types);
  }

  return sharepoint_rest_drupal_processor_list_text($content_types[$value]);
}

/**
 * @param $value
 * @return array
 */
function sharepoint_rest_drupal_processor_list_text($value) {
  $values = explode(';', $value);

  $return_values = array();
  foreach ($values as $value) {
    $machine_name = str_replace(array(' ', '/'), '_', drupal_strtolower($value));
    // Remove characters not valid in function names.
    $return_values[] = array(
      'value' => preg_replace('/[^a-z0-9_]/', '', $machine_name)
    );
  }

  return $return_values;
}

/**
 * @param $value
 * @param $field_info
 * @return array
 */
function sharepoint_rest_drupal_processor_taxonomy($value, $field_info) {
  $values = explode(';', $value);

  $vocabulary_machine_name = $field_info['settings']['allowed_values'][0]['vocabulary'];
  $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_machine_name);

  $return_values = array();
  foreach ($values as $value) {

    $value = trim($value);
    if (empty($value)) {
      continue;
    }

    $taxonomy_term = taxonomy_term_load_multiple(array(), array(
      'name' => $value,
      'vid' => $vocabulary->vid,
    ));

    if (empty($taxonomy_term)) {
      $taxonomy_term = new stdClass();
      $taxonomy_term->vid = $vocabulary->vid;
      $taxonomy_term->name = $value;
      taxonomy_term_save($taxonomy_term);
    }
    else {
      $taxonomy_term = array_shift($taxonomy_term);
    }

    $return_values[] = array(
      'tid' => $taxonomy_term->tid
    );
  }

  return $return_values;
}

/**
 * @param $value
 * @param $field_info
 * @param $map
 * @return array
 */
function sharepoint_rest_drupal_processor_text($value, $field_info, $map) {

  $fields_map = $map['field_map'];
  $field_name = $field_info['field_name'];

  $html_allowed = FALSE;
  foreach ($fields_map as $field_map) {
    if (!empty($field_map['drupal_name']) && $field_map['drupal_name'] == $field_name) {
      $html_allowed = !empty($field_map['html']) ? TRUE : FALSE;
      break;
    }
  }

  if ($html_allowed) {
    $format = variable_get('sharepoint_rest_html_text_format', 'full_html');
  }
  else {
    $format = variable_get('sharepoint_rest_default_text_format', 'filtered_html');
  }

  $return_values = array();
  $return_values[] = array(
    'value' => html_entity_decode($value, ENT_COMPAT, 'UTF-8'),
    'format' => $format,
  );

  return $return_values;
}

function sharepoint_rest_drupal_processor_integer($value) {

  $return_values = array();
  $value = trim($value);
  if (!empty($value) && ctype_digit($value)) {
    $return_values[] = array(
      'value' => $value
    );
  }

  return $return_values;
}

/**
 * @param $value
 * @param $field_info
 * @return array
 */
function sharepoint_rest_drupal_processor_file($value, $field_info) {

  $web = new SharePointRESTWeb();
  $file_content = $web->getFile($value);

  if (empty($file_content)) {
    return array();
  }

  $file_name = drupal_substr($value, strrpos($value, '/') + 1);
  $uri_scheme = !empty($field_info['settings']['uri_scheme']) ? $field_info['settings']['uri_scheme'] : file_default_scheme();
  $file_path_info = pathinfo($file_name);
  $folder = $uri_scheme . '://sharepoint';
  file_prepare_directory($folder, FILE_CREATE_DIRECTORY);

  // TODO: Inject possibility to move files to another folder.
  $file = file_save_data($file_content, $folder . '/' . $file_name);
  if (empty($file)) {
    return array();
  }

  $file->display = 1;
  $file->description = $file_path_info['filename'];

  $return_values = array();
  $return_values[] = (array) $file;
  return $return_values;
}

/**
 * @param $value
 * @param $field_info
 * @param $map
 * @param $entity
 * @return array
 */
function sharepoint_rest_drupal_processor_file_url($value, $field_info, $map, $entity) {
  if (empty($value)) {
    return array();
  }

  $sp_file_urls = explode(', ', $value);
  $sp_file_url = $sp_file_urls[0];

  $drupal_field_name = $field_info['field_name'];
  $field_language = field_is_translatable($map['drupal']['entity_type'], $field_info) ? language_default('language') : LANGUAGE_NONE;

  if (!empty($entity->{$drupal_field_name}[$field_language][0]['uri'])) {
    $uri = $entity->{$drupal_field_name}[$field_language][0]['uri'];
    $file_url = file_create_url($uri);
    if ($file_url == $sp_file_url) {
      return $entity->{$drupal_field_name}[$field_language];
    }
  }

  $file_name = drupal_substr($sp_file_url, strrpos($sp_file_url, '/') + 1);
  $file_path_info = pathinfo($file_name);
  $uri_scheme = !empty($field_info['settings']['uri_scheme']) ? $field_info['settings']['uri_scheme'] : file_default_scheme();

  // If the file lives in sharepoint then we need to use a client that has the authentication configured already.
  if (strstr($sp_file_url, '.sharepoint.com/') !== FALSE) {
    $client = SharePointREST::getClient();
    $options = array(
      'url' => $sp_file_url,
      'method' => 'GET',
    );
    $file_content = $client->request($options);
  }
  else {
    $file_content = file_get_contents($sp_file_url);
  }
  $folder = $uri_scheme . '://sharepoint';
  file_prepare_directory($folder, FILE_CREATE_DIRECTORY);

  // TODO: Inject possibility to move files to another folder.
  $file = file_save_data($file_content, $folder . '/' . $file_name);
  if (empty($file)) {
    return array();
  }

  $file->display = 1;
  $file->description = $file_path_info['filename'];

  // Saves ALT and TITLE properties for images.
  if ($file->type == 'image' && !empty($sp_file_urls[1])) {
    $file->alt = $sp_file_urls[1];
    $file->title = $sp_file_urls[1];
  }

  $return_values = array();
  $return_values[] = (array) $file;
  return $return_values;
}

/**
 * @param $value
 * @return int
 */
function sharepoint_rest_drupal_processor_date($value, $field_info, $map, $entity) {

  if (empty($value)) {
    return REQUEST_TIME;
  }

  if (!empty($entity->created)) {
    return $entity->created;
  }

  $timezone = date_default_timezone();
  $date = DateTime::createFromFormat('d/m/Y H:i', $value, new DateTimeZone($timezone));
  return $date->getTimestamp();
}

/**
 * @param $value
 * @return int
 */
function cw_sharepoint_drupal_processor_publish_status($value) {
  return $value == 'Approved' ? NODE_PUBLISHED : NODE_NOT_PUBLISHED;
}
