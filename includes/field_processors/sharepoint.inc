<?php

/**
 * Status within Sharepoint : 0 = Approved, 1 = Rejected, 2 = Pending
 */
function cw_sharepoint_sharepoint_processor_publish_status($value) {
  return $value == NODE_PUBLISHED ? 0 : 1;
}

function sharepoint_rest_sharepoint_processor_taxonomy($value, $list_type, $field_info, $field_map) {

  if (empty($value[LANGUAGE_NONE])) {
    return '';
  }

  // Load drupal terms.
  $return_values = array();
  $tids = array();
  foreach ($value[LANGUAGE_NONE] as $key => $tid) {
    $tids[] = $tid['tid'];
  }
  $terms = taxonomy_term_load_multiple($tids);

  // TODO: Better algorithm needed.
  if (!empty($field_map['taxonomy_multiple'])) {

    $result = call_user_func($field_map['taxonomy_map']);
    $sp_terms = array();
    foreach ($result as $sp_term_name => $sp_term) {
      $sp_term['name'] = $sp_term_name;
      $sp_term_name = drupal_strtolower($sp_term_name);
      $sp_terms[$sp_term_name] = $sp_term;
    }

    foreach ($terms as $term) {
      foreach ($sp_terms as $sp_term_name_lowercase => $sp_term) {
        if ($sp_term_name_lowercase == strtolower($term->name)) {
          // "-1;#World Region 1|b23b6538-af4a-4ab4-b338-496cda1055f4;#-1;#World Region 2|fd238d90-05fb-4dd2-8132-0a9757cd7c6a;";
          $return_values[] = '-1;#' . $sp_term['name'] . '|' . $sp_term['uuid'] . ';';
        }
      }
    }
  }
  else {
    // Single taxonomy term.
    /* $_metadata = new stdClass();
    $_metadata->type = 'SP.Taxonomy.TaxonomyFieldValue';

   $sp_item_properties['Theme'] = array(
      '__metadata' => $_metadata,
      'Label' => '20',
      'TermGuid' => '7cf77c90-3c70-496f-be2f-b28c6c75ebed',
      'WssId' => -1,
    );
    $sp_item_properties['gc4732adc7b14430b477778f19f3f1a7'] = '-1;#20|7cf77c90-3c70-496f-be2f-b28c6c75ebed;';
  */
  }

  return implode('#', $return_values);
}

function sharepoint_rest_sharepoint_processor_file_url($value) {

  $metadata = new stdClass();
  $metadata->type = 'SP.FieldUrlValue';

  $url = '';
  if (!empty($value[LANGUAGE_NONE][0]['uri'])) {
    $url = file_create_url($value[LANGUAGE_NONE][0]['uri']);
  }

  $description = '';
  if (!empty($value[LANGUAGE_NONE][0]['title'])) {
    $description = $value[LANGUAGE_NONE][0]['title'];
  }

  return array(
    '__metadata' => $metadata,
    'Description' => $description,
    'Url' => $url,
  );
}

function sharepoint_rest_sharepoint_processor_text($value) {
  return $value[LANGUAGE_NONE][0]['value'];
}
