<?php

/**
 * @file
 * Contains REST API methods for integration with SharePoint.
 */

/**
 * Wraps SPO Client to add new methods to the REST API.
 */
class SharePointRESTWeb  {

  protected $endpoint = '/_api/web';

  protected $service;


  public function __construct() {
    $this->service = SharePointREST::getClient();
  }


  /**
   * @see https://msdn.microsoft.com/en-us/library/jj246733.aspx
   */
  public function getLists() {
    $options['endpath'] = 'lists';
    $response = $this->request($options);
    return !empty($response->d->results) ? $response->d->results : FALSE;
  }

  /**
   * @see https://msdn.microsoft.com/EN-US/library/dn292553.aspx
   */
  public function getFile($file_path) {
    $options['endpath'] = 'GetFileByServerRelativeUrl(\'' . rawurlencode(str_replace("'", "''", $file_path)) . '\')/$value';
    $response = $this->request($options);
    return !empty($response) ? $response : FALSE;
  }


  public function getFileFields($file_path) {
    $options['endpath'] = 'GetFileByServerRelativeUrl(\'' . rawurlencode(str_replace("'", "''", $file_path)) . '\')/ListItemAllFields';
    $response = $this->request($options);
    return !empty($response->d) ? $response->d : FALSE;
  }


  public function getRecycleBin() {
    $options['endpath'] = 'RecycleBin?&expand=leafName';
    return $this->request($options);
  }


  protected function request($options = array()) {

    $options += array(
      'endpoint' => $this->endpoint,
      'method' => 'GET',
    );

    return $this->service->request($options);
  }

}
