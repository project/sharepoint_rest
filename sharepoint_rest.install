<?php

/**
 * @file
 * Contains install / uninstall functions for the module.
 */

/**
 * Implements hook_schema().
 */
function sharepoint_rest_schema() {

  $schema['sharepoint_rest_map'] = array(
    'description' => 'The table contains mapping between Drupal and Sharepoint entities.',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'entity_type' => array(
        'description' => 'Drupal entity type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'entity_id' => array(
        'description' => 'Drupal entity id.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'sp_list_type' => array(
        'description' => 'List Type of SharePoint item.',
        'type' => 'char',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ),
      'sp_item_uuid' => array(
        'description' => 'UUID of SharePoint item.',
        'type' => 'char',
        'length' => 36,
        'not null' => TRUE,
        'default' => '',
      ),
      'sp_item_id' => array(
        'description' => 'ID of SharePoint item.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The timestamp when the map was created.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'drupal_entity' => array('entity_type', 'entity_id'),
      'sp_item' => array('sp_list_type', 'sp_item_id'),
    ),
    'unique keys' => array(
      'entity_id_sp_uuid' => array('sp_item_uuid', 'entity_type', 'entity_id'),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}
