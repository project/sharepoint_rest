<?php

/**
 * @file
 *
 */

/**
 * TODO
 */
function sharepoint_rest_migrate_to_drupal($data, $map, $entity = NULL) {

  if (empty($map['sharepoint']['list_type']) || empty($map['drupal']['entity_type']) || empty($map['drupal']['entity_bundle'])) {
    return FALSE;
  }

  $entity_type = $map['drupal']['entity_type'];
  $entity_bundle = $map['drupal']['entity_bundle'];
  $list_name = $map['sharepoint']['list_type'];

  $entity_values = array(
    'type' => $entity_bundle,
    'bundle' => $entity_bundle,
    'language' => language_default('language'),
    'uid' => variable_get('sharepoint_rest_default_uid', 1),
  );

  if (empty($entity)) {
    $entity = entity_create($entity_type, $entity_values);
  }

  if (!empty($map['file']) && is_file($map['file'])) {
    require_once $map['file'];
  }

  if (!empty($map['field_map'])) {
    foreach ($map['field_map'] as $field_name => $field_map) {

      $sp_field_name = !empty($field_map['sharepoint_name']) ? $field_map['sharepoint_name'] : $field_name;
      $drupal_field_name = !empty($field_map['drupal_name']) ? $field_map['drupal_name'] : $field_name;

      // Strip out dirty SharePoint chars.
      $dirty_chars = variable_get('sharepoint_rest_dirty_chars', array('x005f_', 'x0020_', 'OData_'));
      $sp_field_name = str_replace($dirty_chars, '', $sp_field_name);

      if (!isset($data[$sp_field_name])) {
        SharePointRest::logError(t('Field mapping for the list "' . $list_name . '" has the field "' . $sp_field_name . '", but it is not represented in the output.'));
        continue;
      }

      $field_value = $data[$sp_field_name];
      $field_info = empty($field_map['property']) ? field_info_field($drupal_field_name) : FALSE;

      if (!empty($field_map['processor'])) {
        $field_value = call_user_func($field_map['processor'], $field_value, $field_info, $map, $entity);
      }

      if (!empty($field_map['property'])) {
        $entity->{$drupal_field_name} = $field_value;
      }
      else {
        // Use entity language for translatable fields.
        $field_language = field_is_translatable($entity_type, $field_info) ? $entity_values['language'] : LANGUAGE_NONE;

        if (empty($field_map['reused_field'])) {
          $entity->{$drupal_field_name}[$field_language] = $field_value;
        }
        elseif (!empty($field_value)) {
          $entity->{$drupal_field_name}[$field_language] = array_merge($entity->{$drupal_field_name}[$field_language], $field_value);
        }
      }
    }
  }

  // Set a flag to know it was saved from sync.
  $entity->sharepoint_migrate_flag = TRUE;
  entity_save($entity_type, $entity);
  return $entity;
}

/**
 * TODO: Describe.
 */
function sharepoint_rest_migrate_to_sharepoint($entity_id, $entity_map, $sp_item = NULL) {

  if (empty($entity_map['sharepoint']['list_type']) || empty($entity_map['drupal']['entity_type'])) {
    return FALSE;
  }

  $entity_type = $entity_map['drupal']['entity_type'];
  $list_name = $entity_map['sharepoint']['list_type'];

  if (!empty($entity_map['file']) && is_file($entity_map['file'])) {
    require_once $entity_map['file'];
  }

  $entity = entity_load_single($entity_type, $entity_id);

  $sp_item_properties = array();
  foreach ($entity_map['field_map'] as $field_name => $field_map) {

    $sp_field_name = !empty($field_map['sharepoint_name']) ? $field_map['sharepoint_name'] : $field_name;
    $drupal_field_name = !empty($field_map['drupal_name']) ? $field_map['drupal_name'] : $field_name;

    if (!empty($field_map['constant'])) {
      $field_value = $field_map['value'];
    }
    else {
      $field_value = $entity->{$drupal_field_name};
    }

    if (!empty($field_map['processor'])) {
      $field_info = field_info_field($drupal_field_name);
      $field_value = call_user_func($field_map['processor'], $field_value, $list_name, $field_info, $field_map);
    }

    $sp_item_properties[$sp_field_name] = $field_value;
  }

  if (!empty($entity_map['meta']['file_field'])) {
    $field_items = field_get_items($entity_type, $entity, $entity_map['meta']['file_field']);
  }

  if (isset($sp_item_properties['OData__ModerationStatus'])) {
    $moderation_status = $sp_item_properties['OData__ModerationStatus'];
    unset($sp_item_properties['OData__ModerationStatus']);
  }

  if (empty($field_items) || !is_array($field_items)) {
    // TODO: UNTESTED CASE.
    $sp_item = new SharePointRESTListItem($list_name);
    $sp_item = $sp_item->create($sp_item_properties);

    if (!empty($sp_item) && !empty($sp_item->GUID)) {
      sharepoint_rest_api_add_map($entity_type, $entity_id, $sp_item->GUID, $list_name, $sp_item->Id);
      if (isset($moderation_status)) {
        $sp_item->update($sp_item->Id, array('OData__ModerationStatus' => $moderation_status));
      }
    }
    else {
      watchdog('sharepoint_rest', 'Could not migrate %entity_type with ID %id to SharePoint.', array(
        '%entity_type' => $entity_type,
        '%entity_id' => $entity_id,
      ));
      return FALSE;
    }
  }
  else {
    foreach ($field_items as $field_item) {

      $file_content = file_get_contents($field_item['uri']);
      $sp_item = new SharePointRESTListItem($list_name);
      $item = $sp_item->createWithFile($field_item['filename'], $file_content, $sp_item_properties);

      if (!empty($item) && !empty($item->GUID)) {
        sharepoint_rest_api_add_map($entity_type, $entity_id, $item->GUID, $list_name, $item->Id);

        if (isset($moderation_status)) {
          $sp_item->update($item->Id, array('OData__ModerationStatus' => $moderation_status));
        }
      }

    }
  }

  return $sp_item;
}

/**
 * TODO: Describe.
 */
function sharepoint_rest_sp_to_drupal_maps($cron_sync = FALSE) {
  $map = module_invoke_all('sharepoint_rest_sp_to_drupal_mapping');
  $available_maps = array();
  foreach ($map as $map_entry) {
    if (empty($map_entry['sharepoint']['list_type']) || empty($map_entry['drupal']['entity_type']) || empty($map_entry['drupal']['entity_bundle'])) {
      continue;
    }

    if (empty($cron_sync) || (!empty($cron_sync) && !empty($map_entry['cron_sync']))) {
      $available_maps[] = $map_entry;
    }
  }

  return $available_maps;
}

/**
 * TODO: Describe.
 */
function sharepoint_rest_drupal_to_sp_maps() {
  $map = module_invoke_all('sharepoint_rest_drupal_to_sp_mapping');
  $available_maps = array();
  foreach ($map as $map_entry) {
    if (empty($map_entry['sharepoint']['list_type']) || empty($map_entry['drupal']['entity_type']) || empty($map_entry['drupal']['entity_bundle'])) {
      continue;
    }
    $available_maps[] = $map_entry;
  }

  return $available_maps;
}

/**
 * TODO: Describe
 */
function sharepoint_rest_api_add_map($entity_type, $entity_id, $sp_uuid, $sp_list, $sp_id) {
  db_merge('sharepoint_rest_map')
    ->key(array(
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
      'sp_list_type' => $sp_list,
      'sp_item_uuid' => $sp_uuid,
      'sp_item_id' => $sp_id,
    ))
    ->fields(array(
      'created' => REQUEST_TIME
    ))
    ->execute();
}

/**
 * TODO: Describe.
 */
function sharepoint_rest_api_get_map($entity_type, $entity_id) {
  $result = db_select('sharepoint_rest_map')
    ->fields('sharepoint_rest_map', array('sp_item_uuid', 'sp_list_type', 'sp_item_id'))
    ->condition('entity_type', $entity_type)
    ->condition('entity_id', $entity_id)
    ->orderBy('created', 'DESC')
    ->execute();

  $sp_items = array();
  foreach ($result as $entity) {
    $sp_items[] = $entity;
  }

  return $sp_items;
}

/**
 * TODO: Describe
 */
function sharepoint_rest_api_get_map_by_uuid($sharepoint_uuid) {
  $result = db_select('sharepoint_rest_map')
    ->fields('sharepoint_rest_map', array('entity_type', 'entity_id'))
    ->condition('sp_item_uuid', $sharepoint_uuid)
    ->orderBy('created', 'DESC')
    ->execute();

  $entities = array();
  foreach ($result as $entity) {
    $entities[] = $entity;
  }

  return $entities;
}

/**
 * TODO: Describe
 */
function sharepoint_rest_api_get_map_by_sp_item($list_type, $item_id) {
  $result = db_select('sharepoint_rest_map')
    ->fields('sharepoint_rest_map', array('entity_type', 'entity_id'))
    ->condition('sp_list_type', $list_type)
    ->condition('sp_item_id', $item_id)
    ->orderBy('created', 'DESC')
    ->execute();

  $entities = array();
  foreach ($result as $entity) {
    $entities[] = $entity;
  }

  return $entities;
}

/**
 * TODO: Describe
 */
function sharepoint_rest_api_delete_map($entity_type, $entity_id) {
  db_delete('sharepoint_rest_map')
    ->condition('entity_type', $entity_type)
    ->condition('entity_id', $entity_id)
    ->execute();
}
